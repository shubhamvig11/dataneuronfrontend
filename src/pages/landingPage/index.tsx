import React, { useEffect, useState } from 'react';
import styles from '../../styles/modules/landingpage.module.css';
import ResizableContainer from '@/component/MyResizableComponent';
import axios from 'axios';

const LandingPage = () => {
  // State hooks to manage form fields, error, and API call count
  const [first, setFirst] = useState<any>('');
  const [second, setSecond] = useState<any>('');
  const [third, setThird] = useState<any>('');
  const [error, setError] = useState<any>('');
  const [collectionid, setCollectionid] = useState<any>('');
  const [apiCalled, setApiCalled] = useState<number>(0);

  // Handler for resizing the ResizableContainer
  const handleResize = (width: any, height: any) => {
    console.log(`New size: ${width} x ${height}`);
  };

  // Handler for input change based on inputID
  const handleInputChange = (inputID: number, value: string) => {
    switch (inputID) {
      case 0:
        setFirst(value);
        break;
      case 1:
        setSecond(value);
        break;
      case 2:
        setThird(value);
        break;
      default:
        break;
    }
  };

  // API call to add new fields
  const handleAddButtonClick = async () => {
    try {
      if (first && second && third) {
        await axios.post('http://localhost:5000/api/add', { input1: first, input2: second, input3: third });
        // Clear input fields after successful add operation
        let count = apiCalled + 1;
        setApiCalled(count);
      } else {
        setError('Kindly fill all fields');
      }
    } catch (error) {
      console.error('Error adding data:', error);
    }
  };

  // API call to update existing fields
  const handleUpdateButtonClick = async () => {
    try {
      if (collectionid && first && second && third) {
        await axios.put('http://localhost:5000/api/update', {
          input1: first,
          input2: second,
          input3: third,
          collectionid: collectionid
        });
        let count = apiCalled + 1;
        setApiCalled(count);
        console.log("Update successful!");
      } else {
        setError('Kindly fill all fields');
      }
    } catch (error) {
      console.error('Error updating data:', error);
    }
  };

  // API call to retrieve data on component mount
  const getData = async () => {
    try {
      const result = await axios.get('http://localhost:5000/api/get');
      setFirst(result?.data?.record?.input1);
      setSecond(result?.data?.record?.input2);
      setThird(result?.data?.record?.input3);
      setCollectionid(result?.data?.record?._id);
    } catch (error) {
      console.error('Error fetching data:', error);
    }
  };

  // Effect hook to retrieve data when component mounts
  useEffect(() => {
    getData();
  }, []);

  return (
    <>
      {/* Buttons to trigger API calls */}
      <button onClick={handleAddButtonClick}>Add</button>
      <button onClick={handleUpdateButtonClick}>Update</button>
      
      {/* Display number of API calls */}
      <h2>Api Called: {apiCalled}</h2>

      {/* Display error message if any */}
      {error && <p style={{ color: "red" }}>{error}</p>}

      {/* Container for resizable components */}
      <div className={styles.mainContainer}>
        {/* ResizableContainer components with different keys and properties */}
        <ResizableContainer
          key={0}
          initialWidth={500}
          initialHeight={200}
          onResize={handleResize}
          text={first}
          inputID={0}
          onHandle={handleInputChange}
        />
        <ResizableContainer
          key={1}
          initialWidth={300}
          initialHeight={200}
          onResize={handleResize}
          text={second}
          inputID={1}
          onHandle={handleInputChange}
        />
        <ResizableContainer
          key={2}
          initialWidth={300}
          initialHeight={200}
          onResize={handleResize}
          text={third}
          inputID={2}
          onHandle={handleInputChange}
        />
      </div>
    </>
  );
};

export default LandingPage;
