import type { NextPage } from 'next'
import Image from 'next/image'
import { Fragment } from 'react'
import LandingPage from './landingPage'


const Home: NextPage = () => {

    return (
        <Fragment>
            <LandingPage />
        </Fragment>
    )
}

export default Home
