import type { AppProps } from 'next/app'
import { Fragment, useEffect, useState } from 'react'
import { useRouter } from 'next/router'
import Head from 'next/head';
import pageRoutes from '@/layout/routing';



function MyApp({ Component, pageProps }: AppProps) {
  const router = useRouter()


  return (
    <Fragment>
      {
        pageRoutes.map((item: any, index) => {
          if (router.pathname === item.pageName) {
            return (
              <>
                  <Component {...pageProps} />              
              </>
            )
          }
        })
      }
    </Fragment>
  )
}

export default MyApp