import React, { useEffect, useState } from 'react';
import styles from '../styles/modules/resizable.module.css';
import axios from 'axios';



const ResizableContainer = ({ initialWidth, initialHeight, onResize, text, inputID, onHandle }: any) => {
    const [width, setWidth] = useState(initialWidth);
    const [height, setHeight] = useState(initialHeight);
    const [inputText, setInputText] = useState<any>([
        { id: 0, text: "" },
        { id: 1, text: "" },
        { id: 2, text: "" }
    ]);

    // Fetch data from API on component mount
    const getData = async () => {
        try {
            const result = await axios.get('http://localhost:5000/api/get');
            const record = result?.data?.record;

            if (record) {
                const updatedInputText = [...inputText]; // Create a copy of the current state

                if (record.input1) {
                    updatedInputText[0].text = record.input1; // Update input1 value
                }
                if (record.input2) {
                    updatedInputText[1].text = record.input2; // Update input2 value
                }
                if (record.input3) {
                    updatedInputText[2].text = record.input3; // Update input3 value
                }

                setInputText(updatedInputText); // Update the state with the modified array
            }
        } catch (error) {
            console.error('Error fetching data:', error);
        }
    };

    // Effect hook to fetch data on component mount
    useEffect(() => {
        getData();
    }, []);

    // Handler for mouse down event on resize handles
    const handleMouseDown = (e: any, direction: any) => {
        e.preventDefault();
        const startX = e.clientX;
        const startY = e.clientY;
        const startWidth = width;
        const startHeight = height;

        const handleMouseMove = (e: any) => {
            const deltaX = e.clientX - startX;
            const deltaY = e.clientY - startY;

            let newWidth = startWidth;
            let newHeight = startHeight;

            switch (direction) {
                case 'right':
                    newWidth = startWidth + deltaX;
                    break;
                case 'left':
                    newWidth = startWidth - deltaX;
                    break;
                case 'bottom':
                    newHeight = startHeight + deltaY;
                    break;
                case 'top':
                    newHeight = startHeight - deltaY;
                    break;
                case 'bottom-right':
                    newWidth = startWidth + deltaX;
                    newHeight = startHeight + deltaY;
                    break;
                case 'bottom-left':
                    newWidth = startWidth - deltaX;
                    newHeight = startHeight + deltaY;
                    break;
                case 'top-right':
                    newWidth = startWidth + deltaX;
                    newHeight = startHeight - deltaY;
                    break;
                case 'top-left':
                    newWidth = startWidth - deltaX;
                    newHeight = startHeight - deltaY;
                    break;
                default:
                    break;
            }

            setWidth(newWidth);
            setHeight(newHeight);
            onResize(newWidth, newHeight); // Notify parent about the new size
        };

        const handleMouseUp = () => {
            document.removeEventListener('mousemove', handleMouseMove);
            document.removeEventListener('mouseup', handleMouseUp);
        };

        document.addEventListener('mousemove', handleMouseMove);
        document.addEventListener('mouseup', handleMouseUp);
    };

    // Handler for input change event
    const handleInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const newText = e.target.value;
        const updatedInputText = [...inputText]; // Create a copy of the inputText array
        const inputIndex = updatedInputText.findIndex((item: any) => item.id === inputID);
    
        if (inputIndex !== -1) {
            // Update the text value of the corresponding input object
            updatedInputText[inputIndex].text = newText;
            setInputText(updatedInputText); // Update the state with the modified array
            onHandle(inputID, newText); // Invoke the onHandle function with inputID and newText
        }
    };
    
    return (
        <div className={styles.resizableContainer} style={{ width, height }}>
            {/* Resize handles for different directions */}
            <div className={`${styles.resizeHandle} ${styles.bottom}`} onMouseDown={(e) => handleMouseDown(e, 'bottom')} />
            <div className={`${styles.resizeHandle} ${styles.bottomRight}`} onMouseDown={(e) => handleMouseDown(e, 'bottom-right')} />
            <div className={`${styles.resizeHandle} ${styles.right}`} onMouseDown={(e) => handleMouseDown(e, 'right')} />
            <div className={`${styles.resizeHandle} ${styles.left}`} onMouseDown={(e) => handleMouseDown(e, 'left')} />
            <div className={`${styles.resizeHandle} ${styles.top}`} onMouseDown={(e) => handleMouseDown(e, 'top')} />
            <div className={`${styles.resizeHandle} ${styles.topRight}`} onMouseDown={(e) => handleMouseDown(e, 'top-right')} />
            <div className={`${styles.resizeHandle} ${styles.topLeft}`} onMouseDown={(e) => handleMouseDown(e, 'top-left')} />

            {/* Input field to display and update text */}
            <input
                type="text"
                id={inputID}
                value={inputText?.find((item: any) => item.id === inputID)?.text || ''}
                onChange={handleInputChange}
            />
        </div>
    );
};

export default ResizableContainer;
